# Telemac2d project ( bumpcri example distributed with the Telemac system)

Small project to illustrate how to use a Continuous Integration (CI) Server to launch simulations.
Each time a file is committed and pushed to this repository, a telemac2d computation is launched.
The process is configured thanks to this file [.gitlab-ci.yml](https://gitlab.com/capedev-labs/telemac-examples-bumpcri/blob/master/.gitlab-ci.yml)

Could be easily adapted to Jenkins, Bitbucket,...

## Log file and result files
* in the menu CI/CD, click on the link ["Pipelines"](https://gitlab.com/capedev-labs/telemac-examples-bumpcri/pipelines).
* All the runs will be displayed
* Click on a green pipeline :) (for instance [this](https://gitlab.com/capedev-labs/telemac-examples-bumpcri/pipelines/43919755)) and on the build [telemac2d](https://gitlab.com/capedev-labs/telemac-examples-bumpcri/-/jobs/147918573)
* You will see the log of the computation
* On the right column, there is a [Browse link](https://gitlab.com/capedev-labs/telemac-examples-bumpcri/-/jobs/147918573/artifacts/browse) to download the result files



